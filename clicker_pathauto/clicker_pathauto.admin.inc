<?php
/**
 * @file
 * Clicker pathauto admin functions.
 */

/**
 * Implements hook_pathauto().
 */
function clicker_pathauto_admin_pathauto($op) {
  switch ($op) {
    case 'settings':
      $settings = array(
        'module' => 'clicker_pathauto',
        'groupheader' => t('Clicker paths'),
        'patterndescr' => t('Default clicker path pattern (applies to all link field instances with clicker pathauto enabled)'),
        'batch_update_callback' => 'clicker_pathauto_admin_pathauto_bulkupdate',
        'batch_file' => drupal_get_path('module', 'clicker_pathauto') . '/clicker_pathauto.admin.inc',
        // Token types are altered by form alter.
        // @see clicker_pathauto_form_pathauto_patterns_form_alter().
        'token_type' => 'clicker', 
        'patterndefault' => '[clicker:entity_type]/[clicker:entity_id]/[clicker:field_name]/[clicker:delta]', 
      );
      $token_types = &drupal_static('clicker_pathauto_token_types');
      if (!isset($token_types)) {
        $token_types = array();
      }
      foreach (clicker_get_enabled_field_instances() as $entity_type => $bundles) {
        foreach ($bundles as $bundle_name => $enabled_fields) {
          foreach ($enabled_fields as $field_name => $instance) {
            // Pathauto is enabled for field.
            $key = clicker_pathauto_get_pattern_identifier($field_name, $entity_type, $bundle_name);
            $replacements = array('@field_name' => $field_name, '@entity_type' => $entity_type, '@bundle_name' => $bundle_name);
            $settings['patternitems'][$key] = t('Clicker path pattern for @field_name fields on @bundle_name @entity_type entities.', $replacements);
            $token_types[$key] = $entity_type;
          }
        }
      }
      return (object) $settings;

    default:
      break;
  }
}

/**
 * Clicker pathauto bulk update callback.
 */
function clicker_pathauto_admin_pathauto_bulkupdate() {
  $count = 0;
  foreach (clicker_get_enabled_field_instances() as $entity_type => $bundles) {
    foreach ($bundles as $bundle_name => $enabled_fields) {
      foreach ($enabled_fields as $field_name => $instance) {
        $query = new EntityFieldQuery();
        $query->entityCondition('bundle', $bundle_name)
          ->fieldCondition($field_name, 'url', 'NULL', '!=');

        if ($result = $query->execute()) {
          $entity_ids = array_keys($result[$entity_type]);
          foreach (entity_load($entity_type, $entity_ids) as $entity) {
            clicker_pathauto_create_alias($entity_type, $entity, 'bulkupdate');
            ++$count;
          }
        }
      }
    }
  }
  drupal_set_message(t('Updated @count clicker path(s).', array('@count' => $count)), 'status'); 
}
