<?php
/**
 * @file
 * Page callbacks for the clicker module.
 */
 
/**
 * Menu callback: Redirect to link URL.
 *
 * @param string $entity_type
 * @param int $entity_id
 * @param string $field_name
 * @param int $delta
 */
function clicker_click($entity_type, $entity_id, $field_name, $delta) {
  // Load URL from entity.
  $entity = entity_load_single($entity_type, $entity_id);
  $entity_wrapper = entity_metadata_wrapper($entity_type, $entity);
  // Get field value.
  $field_value = $entity_wrapper->{$field_name}->value();

  // Get requested url.
  if (!$delta && !empty($field_value['url'])) {
    $url = link_cleanup_url($field_value['url']);
  }
  elseif (isset($field_value[$delta]) && !empty($field_value[$delta]['url'])) {
    $url = link_cleanup_url($field_value[$delta]['url']);
  }
  else {
    $url = FALSE;
  }

  // The function to call when redirecting.
  $callback = 'drupal_goto';
  $params = array($url);

  // Allow other modules to do their thing prior to redirecting.
  $context = array(
    'entity_type' => $entity_type,
    'entity' => $entity,
    'field_name' => $field_name,
    'delta' => $delta,
    'url' => $url,
  );
  foreach (module_implements('clicker_click') as $module) {
    $function = $module . '_clicker_click';
    $function($callback, $params, $context);
  }

  // Handle JSON requests.
  if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest')) {
    $result = array(
      'url' => $url,
    );
    return drupal_json_output($result);
  }
  // Redirect to URL (unless callback is altered).
  return call_user_func_array($callback, $params);
}
