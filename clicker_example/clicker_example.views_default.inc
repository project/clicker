<?php
/**
 * @file
 * clicker_example.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function clicker_example_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'clicker_example_click_log';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Click log';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Click log';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Clicker: Clicks */
  $handler->display->display_options['relationships']['clicker_field_clicker_example_link']['id'] = 'clicker_field_clicker_example_link';
  $handler->display->display_options['relationships']['clicker_field_clicker_example_link']['table'] = 'field_data_field_clicker_example_link';
  $handler->display->display_options['relationships']['clicker_field_clicker_example_link']['field'] = 'clicker_field_clicker_example_link';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Content';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Links */
  $handler->display->display_options['fields']['field_clicker_example_link']['id'] = 'field_clicker_example_link';
  $handler->display->display_options['fields']['field_clicker_example_link']['table'] = 'field_data_field_clicker_example_link';
  $handler->display->display_options['fields']['field_clicker_example_link']['field'] = 'field_clicker_example_link';
  $handler->display->display_options['fields']['field_clicker_example_link']['label'] = 'Link title';
  $handler->display->display_options['fields']['field_clicker_example_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_clicker_example_link']['type'] = 'link_title_plain';
  $handler->display->display_options['fields']['field_clicker_example_link']['group_column'] = 'title';
  $handler->display->display_options['fields']['field_clicker_example_link']['group_columns'] = array(
    'url' => 'url',
  );
  $handler->display->display_options['fields']['field_clicker_example_link']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_clicker_example_link']['delta_offset'] = '0';
  /* Field: Content: Links */
  $handler->display->display_options['fields']['field_clicker_example_link_1']['id'] = 'field_clicker_example_link_1';
  $handler->display->display_options['fields']['field_clicker_example_link_1']['table'] = 'field_data_field_clicker_example_link';
  $handler->display->display_options['fields']['field_clicker_example_link_1']['field'] = 'field_clicker_example_link';
  $handler->display->display_options['fields']['field_clicker_example_link_1']['label'] = 'URL';
  $handler->display->display_options['fields']['field_clicker_example_link_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_clicker_example_link_1']['alter']['text'] = '[field_clicker_example_link_1-url]';
  $handler->display->display_options['fields']['field_clicker_example_link_1']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_clicker_example_link_1']['group_column'] = 'url';
  $handler->display->display_options['fields']['field_clicker_example_link_1']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_clicker_example_link_1']['delta_offset'] = '0';
  /* Field: COUNT(Clicker: Cid) */
  $handler->display->display_options['fields']['cid']['id'] = 'cid';
  $handler->display->display_options['fields']['cid']['table'] = 'clicker_log';
  $handler->display->display_options['fields']['cid']['field'] = 'cid';
  $handler->display->display_options['fields']['cid']['relationship'] = 'clicker_field_clicker_example_link';
  $handler->display->display_options['fields']['cid']['group_type'] = 'count';
  $handler->display->display_options['fields']['cid']['label'] = 'Click count';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Sort criterion: COUNT(Clicker: Cid) */
  $handler->display->display_options['sorts']['cid']['id'] = 'cid';
  $handler->display->display_options['sorts']['cid']['table'] = 'clicker_log';
  $handler->display->display_options['sorts']['cid']['field'] = 'cid';
  $handler->display->display_options['sorts']['cid']['relationship'] = 'clicker_field_clicker_example_link';
  $handler->display->display_options['sorts']['cid']['group_type'] = 'count';
  $handler->display->display_options['sorts']['cid']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'clicker_example' => 'clicker_example',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'click-log';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Click log';
  $handler->display->display_options['menu']['description'] = 'Clicker example click log';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['clicker_example_click_log'] = $view;

  return $export;
}
