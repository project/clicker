<?php
/**
 * @file
 * clicker_example.features.inc
 */

/**
 * Implements hook_views_api().
 */
function clicker_example_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function clicker_example_node_info() {
  $items = array(
    'clicker_example' => array(
      'name' => t('Clicker example content'),
      'base' => 'node_content',
      'description' => t('Clicker example content type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
