Clicker obfuscates Link fields by replacing the original link URL with an internal URL,
that redirects the user to the intended URL.

Installation
------------

This module is dependant on the Link module, which can be downloaded from
https://www.drupal.org/project/link


Configuration
-------------

- To enable clicker, visit the field configuration for a Link field, find
  the "Clicker settings" and check "Enable clicker for this field".

Submodules
-------------

- "Clicker pathauto":
  Clean up clicker urls using pathauto patterns. Enable and configure patterns
  at admin/config/search/path/patterns

- "Clicker log": Logs each click to the database. Logged info contains:
  - Timestamp for click.
  - User that clicked.
  - Entity and version ids for entity containing clicked field.
  - Clicked field name and delta.
  - The click request's remote P address.
  Logged info can be accessed from views. See "Clicker example" for an
  example view.

- "Clicker example": Example feature showcasing clicker and clicker log.
  Enable, create example content and play with the provided "Click log"
  view.