/**
 * @file
 * Get clicker URLs using AJAX.
 */
(function($) {
  Drupal.behaviors.clickerAjax = {
    attach: function (context, settings) {
      // Throbber div.
      var $throbber = $('<div/>').addClass('ajax-progress ajax-progress-throbber').html($('<div/>').addClass('throbber'));  

      $('.clicker-use-ajax a', context).one('click', function(event) {
        event.preventDefault();
        var $this = $(this);

        // Disable href while fetching from AJAX.
        var href = $this.attr('href');
        $this.attr('href', '#');

        // Add throbber.
        var $link_throbber = $throbber.appendTo($this);

        // Get original URL from system.
        $.ajax({
          type: 'GET',
          url: href,
          dataType: 'json',
          success: function(result) {
            // Replace URL with request URL and trigger default click.
            $this.attr('href', result.url).get(0).click();
          },
          error: function(request, status, error) {
            // On error, fall back to default.
            $this.attr('href', href).get(0).click();
          }
        });
      });
    }
  };
})(jQuery);
