<?php
/**
 * @file
 * Definition of clicker_log_views_handler_field_ip.
 */

/**
 * Render a field as an ip address.
 *
 * @ingroup views_field_handlers
 */
class clicker_log_views_handler_field_ip extends views_handler_field {
  function render($values) {
    $value = $this->get_value($values);
    $value = long2ip($value);
    return $this->sanitize_value($value);
  }
}
