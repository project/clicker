<?php
/**
 * @file
 * Definition of clicker_log_views_handler_relationship_field_clicks.
 */

/**
 * Relationship handler for clicker fields.
 *
 * @ingroup views_relationship_handlers
 */
class clicker_log_views_handler_relationship_field_clicks extends views_handler_relationship  {
  /**
   * Called to implement a relationship in a query.
   */
  function query() {
    $this->ensure_my_table();

    $alias = 'clicker_log_' . $this->definition['field_name'];

    $join_data = array(
      'left_table' => $this->table_alias,
      'left_field' => 'entity_id',
      'table' => 'clicker_log',
      'field' => 'entity_id',
      'extra' => "$alias.field_name = '{$this->definition['field_name']}' AND $this->table_alias.entity_type = $alias.entity_type AND $this->table_alias.delta = $alias.delta"
    );

    if (!empty($this->options['required'])) {
      $join_data['type'] = 'INNER';
    }

    $join = new views_join();
    $join->definition = $join_data;
    $join->construct();
    $join->adjusted = TRUE;

    $this->alias = $this->query->add_relationship($alias, $join, 'clicker_log', $this->relationship);
  }
}
