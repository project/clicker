<?php
/**
 * @file
 * Clicker log views integration.
 */

/**
 * Implements hook_views_data().
 */
function clicker_log_views_data() {
  $data['clicker_log'] = array(
    'table' => array(
      'group' => t('Clicker'),
    ),
    'cid' => array(
      'title' => t('Cid'),
      'help' => t('The click ID.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'ip' => array(
      'title' => t('User IP address'),
      'help' => t('IP address of user who clicked link.'),
      'field' => array(
        'handler' => 'clicker_log_views_handler_field_ip',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'ts' => array(
      'title' => t('Timestamp'),
      'help' => t('Click timestamp.'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    'uid' => array(
      'relationship' => array(
        'base' => 'users',
        'base field' => 'uid',
        'handler' => 'views_handler_relationship',
        'label' => t('User'),
        'title' => t('User'),
        'help' => t('The user who clicked the field.'),
      ),
    ),
  );
  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function clicker_log_views_data_alter(&$data) {
  // Add link click relationships for enabled link fields.
  foreach (clicker_get_enabled_field_instances() as $entity_type => $bundles) {
    foreach ($bundles as $bundle => $instances) {
      foreach ($instances as $field_name => $instance) {
        $pseudo_field_name = 'clicker_' . $field_name;
        $left_table = _field_sql_storage_tablename($instance);
        // Field exists, continue.
        if (isset($data[$left_table][$pseudo_field_name])) {
          continue;
        }
        // Add relationship as pseudo-field to entity.
        $replacements = array(
          '!field_name' => $field_name,
        );
        $data[$left_table][$pseudo_field_name]['relationship'] = array(
          'handler' => 'clicker_log_views_handler_relationship_field_clicks',
          'field_name' => $field_name,
          'base' => 'clicker_log',
          'base field' => 'entity_id',
          'label' => t('Clicks on !field_name', $replacements),
          'group' => t('Clicker'),
          'title' => t('Clicks'),
          'help' => t('Relationship to clicks on !field_name links.', $replacements),
        );
      }
    }
  }
}

/**
 * Implements hook_views_handlers().
 */
function clicker_log_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'clicker_log') . '/views',
    ),
    'handlers' => array(
      'clicker_log_views_handler_field_ip' => array(
        'parent' => 'views_handler_field',
      ),
      'clicker_log_views_handler_relationship_field_to_click' => array(
        'parent' => 'views_handler_relationship',
      ),
    ),
  );
}
