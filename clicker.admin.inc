<?php
/**
 * @file
 * Clicker module admin functions.
 */
 
/**
 * Add module settings to field instance form.
 */
function clicker_field_instance_settings_form(&$form, &$form_state) {
  // Add fieldset containing clicker settings.
  $instance = $form['#instance'];

  // Default instance settings.
  $instance_settings = field_info_instance_settings('link_field');
  // Add default settings to current clicker settings.
  $settings = $instance['settings']['clicker'] + $instance_settings['clicker'];

  // Settings container.
  $form['instance']['settings']['clicker'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clicker settings'),
  );
  $form['instance']['settings']['clicker']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable clicker for this field'),
    '#default_value' => $settings['enabled'],
    '#description' => t('Enable to obfuscate link using clicker.'),
  );
  // Shorthand to clicker settings form.
  $clicker_form = &$form['instance']['settings']['clicker']['container'];

  $clicker_form = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="instance[settings][clicker][enabled]"]' => array('checked' => TRUE),
      ),
    ),
    '#parents' => array('instance', 'settings', 'clicker'),
  );

  $clicker_form['use_ajax'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use AJAX for redirection'),
    '#default_value' => $settings['use_ajax'],
    '#description' => t('Redirect links using AJAX.'),
  );

  $context = array(
    'instance' => $instance,
    'settings' => $settings,
  );
  // Allow other modules to add settings to form.
  drupal_alter('clicker_settings_form', $clicker_form, $context);
}
