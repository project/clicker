<?php 
/**
 * @file
 * Token module integration for the clicker module..
 */

/**
 * Implements hook_token_info().
 */
function clicker_token_info() {
  $type = array(
    'name' => t('Clicker'),
    'description' => t('Tokens related to clicker links.'),
    'needs-data' => 'clicker',
  );

  // Tokens for clicker links.
  $clicker['entity_type'] = array(
    'name' => t('Entity type'),
    'description' => t('Clicker link field entity type'),
  );
  $clicker['bundle_name'] = array(
    'name' => t('Bundle name'),
    'description' => t('Clicker link field bundle'),
  );
  $clicker['entity_id'] = array(
    'name' => t('Entity id'),
    'description' => t('Identifier of entity containing clicker link'),
  );
  $clicker['field_name'] = array(
    'name' => t('Field name'),
    'description' => t('Clicker-enabled link field name'),
  );
  $clicker['delta'] = array(
    'name' => t('Delta'),
    'description' => t('Delta of clicker-enabled link'),
  );

  return array(
    'types' => array('clicker' => $type),
    'tokens' => array('clicker' => $clicker),
  );
}

/**
 * Implements hook_tokens().
 */
function clicker_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type == 'clicker' && !empty($data['clicker'])) {
    $clicker = $data['clicker'];

    foreach ($tokens as $name => $original) {
      if (!empty($data['clicker'][$name])) {
        $replacements[$original] = $data['clicker'][$name];
      }
    }
  }
  return $replacements;
}
