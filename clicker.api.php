<?php
/**
 * @file
 * Clicker module API documentation.
 */

/**
 * Register additional clicker field settings.
 *
 * @return array()
 *   Array of additional module default values, keyed
 *   by setting name.
 */
function hook_clicker_settings() {
  // Pathauto setting per clicker instance.
  return array(
    'pathauto' => FALSE,
  );
}

/**
 * Add additional elements to the settings form.
 *
 * @param array &$form
 *   The settings form for a clicker-enabled field.
 * @param array $context
 *   Array of contextual data:
 *   - instance: The field instance associated with this form.
 *   - settings: The current clicker settings for this instance.
 */
function hook_clicker_settings_form_alter(&$form, $context) {
  // Add an option for pathauto support.
  $form['pathauto'] = array(
    '#type' => 'checkbox',
    '#title' => t('Pathauto'),
    '#default_value' => $context['settings']['pathauto'],
    '#description' => t('Enable URL rewriting for this clicker instance.'),
  );
}


/**
 * Additional processing when a clicker field is clicked.
 *
 * @param string $callback
 *   The name of the callback function to execute after
 *   processing is complete. (drupal_goto by default).
 * @param array $params
 *   Array of parameters to provide to $callback.
 * @param array $context
 *   Array of contextual data:
 *   - entity_type: The entity type containing the link field.
 *   - entity: The entity containing the link field.
 *   - field_name: Machine name of the link field.
 *   - delta: The delta of the clicked link.
 *   - url: The URL the user will be redirected to.
 */
function hook_clicker_click(&$callback, &$params, $context) {
  // Set up data to log.
  list($entity_id, $entity_vid, $bundle) = entity_extract_ids($context['entity_type'], $context['entity']);
  $ip = ip_address();
  $data = array(
    'entity_id' => $entity_id,
    'entity_vid' => $entity_vid,
    'entity_type' => $context['entity_type'],
    'field_name' => $context['field_name'],
    'delta' => $context['delta'],
    'uid' => $GLOBALS['user']->uid,
    'ip' => ip2long($ip),
    'ts' => REQUEST_TIME,
  );

  // Add record to db.
  db_insert('clicker_log')
    ->fields($data)
    ->execute();
}
